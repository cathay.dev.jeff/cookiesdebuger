//
//  ViewController.swift
//  CookiesDebuger
//
//  Created by dev1 on 2019/7/9.
//  Copyright © 2019 dev1. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func loginTapped(_ sender: Any) {
        self.__callAPISignIn()
    }
}

extension ViewController
{
    /// Call api 登入.
    final func __callAPISignIn()
    {
        /*
        guard let text: String = _idTextField.text
            else
        {
            // 基本上永遠不會發生
            return __showExceptionAlert(with: "請輸入帳號")
        }
        
        guard let passwd: String = _passwdTextField.text
            else
        {
            // 基本上永遠不會發生
            return __showExceptionAlert(with: "請輸入密碼")
        }
        
        let userId: String = Keychain.rememberId() ?? text
        let params: [String: String] = ["type": "0", "channel": "MML", "username": userId, "password": passwd]
        */
        
        /*
        let userId: String = "E125055686"
        let params: [String: String] = ["type": "0", "channel": "MML", "username": userId, "password": "tp654fu6"]
        */
        
        let userId: String = "H123275049"
        let params: [String: String] = ["type": "0", "channel": "MML", "username": userId, "password": "q1234567"]
        
        APIResponse<SignInModel>.signIn(params)
        { (response: APIResponse<SignInModel>) in

            
            switch response.rtnCode
            {
            case 1:
                print("登入成功")
                self.__handleSignInSuccess(userId)

                
            case -2:
                print("密碼過期")
                //self.__handleSignInSuccessButPasswdExpired(userId)
                
            case -4, -7:
                print("帳號鎖定")
                //self.__handleAccountLocked(response)
                
            default:
                print("登入失敗")
                //self.__handleSignInFail(userId, response: response)
            }
            
        }
    }
}

extension ViewController
{
    /// 處理登入成功.
    ///
    /// - Parameter userId: 登入的 UserId
    final func __handleSignInSuccess(_ userId: String)
    {
        MemoryTemp.singleton.userId = userId
        
        /*
        // 如果 User 有勾選記住帳號
        if _checkbox.on == true
        {
            Keychain.setRememberId(id: userId)
        }
        else
        {
            Keychain.removeRememberId()
        }
        */
        
        //AppDelegate.replaceToSignInSuccessViewController()
        self.replaceToSignInSuccessViewController()
    }
    
    /// 替換 App rootViewController 到 SignInSuccessViewController
    func replaceToSignInSuccessViewController()
    {
        /*
        //
        guard let userId: String = MemoryTemp.singleton.userId
            else
        {
            fatalError("User singin, but no userId ???") // 基本上不會發生
        }
        
        //
        let userIdWithMD5: String = userId.md5()
        
        // 在這台 device 登入過的 User Id, md5 編碼.
        var signInIdsWithMD5: [String]
        if let temp: [String] = UserDefaults.standard.object(forKey: "uSignInIds") as? [String]
        {
            signInIdsWithMD5 = temp
        }
        else
        {
            signInIdsWithMD5 = []
        }
        
        // 這個帳號曾經登入過了, 直接到 SignInSuccessViewController
        if signInIdsWithMD5.contains(userIdWithMD5)
        {
            let mvc: SignInSuccessViewController = SignInSuccessViewController.fromStoryboard()
            shared().replaceRootViewController(mvc)
        }
        // 這個帳號沒在這個 device 登入過, 跳轉到 LoginTypeViewController, 讓 User 設定手勢或生物辨識
        else
        {
            signInIdsWithMD5.append(userIdWithMD5)
            UserDefaults.standard.set(signInIdsWithMD5, forKey: "uSignInIds")
            UserDefaults.standard.synchronize()
            
            let loginTypeMVC: LoginTypeViewController = LoginTypeViewController.fromStoryboard()
            loginTypeMVC.fromFirstSignIn = true
            
            let nav: UINavigationController = UINavigationController(rootViewController: loginTypeMVC)
            
            shared().replaceRootViewController(nav)
        }
        */
        
        self.performSegue(withIdentifier: "xxx", sender: nil)
    }
}
