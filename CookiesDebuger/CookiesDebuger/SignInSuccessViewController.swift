//
//  SignInSuccessViewController.swift
//  CookiesDebuger
//
//  Created by dev1 on 2019/7/10.
//  Copyright © 2019 dev1. All rights reserved.
//

import UIKit

class SignInSuccessViewController: UIViewController {

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        APIResponse<OCI2_XXXX_EmptyModel>.fetch(actionName: "OCI2_0600",
                                                methodName: "npsDone") { _ in }
        
        
        let dispatchGroup = DispatchGroup()
        
        dispatchGroup.enter()
        APIResponse<OCI2_0600_QueryUserInfoModel>.fetch(
            actionName: "OCI2_0600",
            methodName: "queryUserInfo"
        ){ _ in
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        APIResponse<OCI2_0600_QueryLifeStageModel>.fetch(
            actionName: "OCI2_0600",
            methodName: "queryLifeStage"
        ){ _ in
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        APIResponse<OCI2_0700_QuerySuitInsuranceListModel>.fetch(
            actionName: "OCI2_0700",
            methodName: "querySuitInsuranceList"
        ){ _ in
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        APIResponse<OCI2_1400_GetAllMessageModel>.fetch(
            actionName: "OCI2_1400",
            methodName: "getAllMessage"
        ){ _ in
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        APIResponse<OCI2_1100_QueryRepaymentRecordModel>.fetch(
            actionName: "OCI2_1100",
            methodName: "queryRepaymentRecordModel"
        ){ _ in
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        APIResponse<OCI2_0500_QueryCalendarDataModel>.fetch(
            actionName: "OCI2_0500",
            methodName: "queryCalendarData"
        ){ _ in
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        APIResponse<OCI2_0400_QueryInvestPolicyPerformanceModel>.fetch(
            actionName: "OCI2_0400",
            methodName: "queryInvestPolicyPerformance"
        ){ _ in
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {

        }
    }
    

    @IBAction func showWebView(_ sender: Any) {
        print("")
        let webView: MMLWebViewController = MMLWebViewController.fromStoryboard("Main")
        webView.webInfo = (
            "旅平險",
            //"https://online.cathaylife.com.tw/OIWeb/servlet/HttpDispatcher/OIB0_1000/prompt?channel=MML"
        
        //"https://www.cathaylife.com.tw/ocauth/OCWeb/servlet/HttpDispatcher/OCTxBean/execute?ACTION_NAME=OCZ1_1000&METHOD_NAME=doAuthRedirect&type=online&toURL=OIWeb/servlet/HttpDispatcher/OIB0_1000/prompt?channel=MML"
            "https://www.cathaylife.com.tw/ocauth/OCWeb/servlet/HttpDispatcher/OCBX_Z001/query?TRAN_ID=AKG0_0200&channel=MML"
        )
        let nav: UINavigationController = UINavigationController(rootViewController: webView)

        self.present(nav, animated: true, completion: nil)
    }
}

protocol FromStoryboard {}

extension UIViewController: FromStoryboard {}

extension FromStoryboard where Self: UIViewController
{
    static func fromStoryboard(_ name: String? = nil) -> Self
    {
        let storyboardId: String = String(describing: self)
        let storyboardName: String = (name == nil) ? storyboardId : name!
        let storyboard: UIStoryboard = UIStoryboard(name: storyboardName, bundle: nil)
        
        return storyboard.instantiateViewController(withIdentifier: storyboardId) as! Self
    }
}
