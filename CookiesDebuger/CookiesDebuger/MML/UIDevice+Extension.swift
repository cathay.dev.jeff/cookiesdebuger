//
//  UIDevice+Extension.swift
//  MyMobileLife
//
//  Created by dev1 on 2017/8/23.
//  Copyright © 2017年 CathayLife. All rights reserved.
//

import LocalAuthentication
import UIKit

// MARK: Enum

/// 生物辨識 type.
///
/// - none: 無生物辨識, 或是無法取得.
/// - touchID: touchID.
/// - faceID: faceID
public enum DeviceBiometryType: Int
{
    case none, touchID, faceID
}

// MARK: - Static function

extension UIDevice
{
    /// Device 生物辨識 type.
    ///
    /// - Returns: 返回 device 生物辨識 type.
    static func biometryType() -> DeviceBiometryType
    {
        let context: LAContext = LAContext()

        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        {
            if #available(iOS 11.0, *)
            {
                switch context.biometryType
                {
                    case .none: return .none
                    case .touchID: return .touchID
                    case .faceID: return .faceID
                @unknown default:
                    fatalError()
                }
            }

            return .touchID
        }

        return .none
    }
}

extension UIDevice
{
    /// Device 型號.
    ///
    /// - Returns: 返回 Device 型號.
    static func deviceType() -> String
    {
        var systemInfo: utsname = utsname()
        uname(&systemInfo)

        let machineMirror: Mirror = Mirror(reflecting: systemInfo.machine)

        let result: String = machineMirror.children.reduce("")
        { (result: String, element: Mirror.Child) in

            guard let value: Int8 = element.value as? Int8,
                value != 0
            else
            {
                return result
            }

            return result + String(UnicodeScalar(UInt8(value)))
        }

        return result
    }
}

extension UIDevice
{
    /// Device 是否為 iPhoneX
    ///
    /// - Returns: 返回是/否.
    static func iPhoneX() -> Bool
    {
        switch deviceType()
        {
            case "i386", "x86_64":
                return UIScreen.main.fixedCoordinateSpace.bounds.size.height == 812.0

            case "iPhone10,3", "iPhone10,6":
                return true

            default:
                return false
        }
    }
}

extension UIDevice
{
    /// Device 是否支援 faceId
    static func hasFaceId(_: LAContext = LAContext()) -> Bool
    {
        return biometryType() == .faceID
    }
}

extension UIDevice
{
    /// 清除 Cache and Cookies.
    static func cleanCookies()
    {
        if let cookies: [HTTPCookie] = HTTPCookieStorage.shared.cookies
        {
            for cookie: HTTPCookie in cookies
            {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
    }
    
    /// 關掉 Cache and Cookies.
    static func disableCache()
    {
        URLCache.shared.diskCapacity = 0
    }
}
