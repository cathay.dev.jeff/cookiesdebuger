//
//  Copyright (c) 2017年 shinren.pan@gmail.com All rights reserved.
//

import Foundation

/// 開發環境 Type
///
/// - debug: Debug 模式
/// - stage: Stage 模式
/// - preRelease: PreRelease 模式
/// - release: Release 模式
enum EnvironmentType {
    case debug, stage, preRelease, release
}


/// 開發環境
struct Environment {
    
}

// MARK: - Type
extension Environment {
    
    /// 目前開發環境 Type
    static var type: EnvironmentType {
        
        #if ENV_DEBUG
            return .debug
            
        #elseif ENV_STAGE
            return .stage
            
        #elseif ENV_PRERELEASE
            return .preRelease
            
        #else
            return .release
        #endif
    }
}

// MARK: - Host
extension Environment {
    
    /// Host
    static var host: URL {
        
        let host = "www.cathaylife.com.tw"
        
        /*
        switch self.type {
        case .debug:
            host = "twww.cathaylife.com.tw"
            
        case .stage:
            host = "swww.cathaylife.com.tw"
            
        case .preRelease, .release:
            break
        }
        */
        
        guard let result = URL(string: host) else {
            fatalError()
        }
        
        return result
    }
}

extension Environment
{
    /// Host
    static var travelHost: URL {
        
        var host = "https://online.cathaylife.com.tw"
        
        switch self.type {
        case .debug:
            host = "https://tonline.cathaylife.com.tw"
            
        case .stage:
            host = "https://sonline.cathaylife.com.tw"
            
        case .preRelease, .release:
            break
        }
        
        guard let result = URL(string: host) else {
            fatalError()
        }
        
        return result
    }
}

// MARK: - Health Campaign Host
extension Environment
{
    /// Host
    static var campaignHost: URL {
        
        var host = "https://campaign.cathaylife.com.tw"
        
        switch self.type {
        case .debug:
            host = "https://tcampaign.cathaylife.com.tw"
            
        case .stage:
            host = "https://scampaign.cathaylife.com.tw"
            
        case .preRelease, .release:
            break
        }
        
        guard let result = URL(string: host) else {
            fatalError()
        }
        
        return result
    }
}

extension Environment
{
    static var celebrusConnectionUrl: String
    {
        var host = "https://adc-d.cathaybk.com.tw/"
        
        if self.type == .release
        {
            host = "https://adc.cathaybk.com.tw/"
        }
        
        return host
    }
}

// MARK: - API Service
extension Environment {
    
    /// MML API Service
    static var webservice: URLComponents {
        
        guard let result = URLComponents(string: "https://\(Environment.host)/oc/OCWeb/servlet/HttpDispatcher/OCApiTxBean/execute") else {
            fatalError()
        }
        
        return result
    }
    
    /// Campaigns API Service
    static var campaignsWebservice: URL {
        
        guard let result = URL(string: "https://campaigns.cathaylife.com.tw/OAWeb/servlet/HttpDispatcher/OAA0_0001") else {
            fatalError()
        }
        
        return result
    }
    
    /// OCWeb API Service
    static var OCWebservice: URL {
        
        guard let result = URL(string: "https://\(Environment.host)/oc/OCWeb/servlet/HttpDispatcher/OCApiTxBean/execute") else {
            fatalError()
        }
        
        return result
    }
}

// MARK: - 例外 URL
extension Environment {
    
    /// 登入
    static var signInURL: URL {
        
        guard let components = URLComponents(string: "https://\(Environment.host)/oc/OCWeb/servlet/HttpDispatcher/LoginHandler/signon") else {
            fatalError()
        }
        
        guard let result = components.url else {
            fatalError()
        }
        
        return result
    }
    
    /// 登出
    static var signOutURL: URL {
        
        guard let components = URLComponents(string: "https://\(Environment.host)/oc/OCWeb/servlet/HttpDispatcher/LogoutHandler/logout") else {
            fatalError()
        }
        
        guard let result = components.url else {
            fatalError()
        }
        
        return result
    }
}





// MARK: - 一般網址
extension Environment {
    
    /// 首頁
    static let homepageURLString: String = "https://\(Environment.host)/"
    
    /// 旅平險網址
    //static let travelURLString: String = "https://\(Environment.host)/bc/web/m/product/online/travel_mml/index.html"
    static let travelURLString: String = "\(Environment.travelHost)/OIWeb/servlet/HttpDispatcher/OIB0_1000/prompt?channel=MML"

    /// 理賠進度網址
    static let claimsURLString: String = "https://\(Environment.host)/ocauth/OCWeb/servlet/HttpDispatcher/OCTxBean/execute?ACTION_NAME=OCD1_0700&METHOD_NAME=initData1"
    
    /// sqlite 下載網址
    static let sqliteDownloadURLString = "https://\(Environment.host)/oc/static/OC/I2/SQLite/MOBILE_DB_V4.sqlite"
    
    /// 隱私條款網址
    static let privacyURLString: String = "https://\(Environment.host)/oc/OCWeb/html/OC/I2/mmlTerms.html"
    
    /// 會員網站網址
    static var memberURL: URL {
        
        let string = "https://\(Environment.host)/ocauth/OCWeb/servlet/HttpDispatcher/OCTxBean/execute?ACTION_NAME=OCA1_0100&METHOD_NAME=initData"
        
        guard let result = URL(string: string) else {
            fatalError()
        }
        
        return result
    }
    
    /// 線上諮詢網址
    ///
    /// - Parameter userId: 使用者 Id
    /// - Returns: 返回網址, 無 userId 返回 nil
    static func advisoryURLString(_ userId: String?) -> String? {
        
        guard let userId = userId else {
            return nil
        }
        
        return "https://www.cathaylife.com.tw/website/eService/customerSubmit.do?method=mobilechat&operationType=addForm&name=Customer&roci=\(userId)"
    }
    
    /// 預約諮詢網址
    ///
    /// - Parameter userId: 使用者 Id
    /// - Returns: 無 userId 返回 nil
    static func reserveAdvisoryURLString(_ userId: String?) -> String? {
        
        guard let userId = userId else {
            return nil
        }
        
        return "https://\(Environment.host)/website/eService/customerSubmit.do?method=mobilecallback&operationType=addForm&name=Customer&roci=\(userId)&channel=mml"
    }
}
