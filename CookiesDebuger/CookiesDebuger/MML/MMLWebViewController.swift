//
//  MMLWebViewController.swift
//  MyMobileLife
//
//  Created by dev1 on 2017/3/16.
//  Copyright © 2017年 CathayLife. All rights reserved.
//

import UIKit
import WebKit
import UserNotifications


final class MMLWebViewController: UIViewController
{
    var cookiesText: String = "NA"
    
    /// WebView 上一頁
    @IBOutlet private var backItem: UIBarButtonItem!
    
    /// WebView 下一頁
    @IBOutlet private var forwardItem: UIBarButtonItem!
    
    /// WebView Reload
    @IBOutlet private var reloadItem: UIBarButtonItem!
    
    /// WebView Share action
    @IBOutlet var actionItem: UIBarButtonItem!
    
    /// Show Share action flag
    var showActionItem: Bool = true
    
    /// WebView
    private var webView: WKWebView!
    private var configuration = WKWebViewConfiguration()

    /// 網站資訊
    var webInfo: (title: String?, url: String?)?
    
    /// 是否從登入成功後開啟 NPS
    var openNPSFromLoginSuccess: Bool = false
    
    /// Notification 通知是否需要 Reload
    static let ShouldReload: Notification.Name = Notification.Name("ShouldReload")

    /// 是否需要 Reload
    private var shouldReload: Bool = false

    /// 應該 Reload 的 URL 關鍵字
    private var shouldReloadList: [String] = ["OCC1_0320", "OCZ1_0500", "WIN0_"]

    /// 載入狀況 view
    @IBOutlet private var loadingView: UIActivityIndicatorView!

    /// 回到上一頁時重載
    var shouldReloadWhenGoBackPreviousPage: Bool = false

    // MARK: - LifeCycle

    override func loadView()
    {
        super.loadView()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.setupWKWebView()
        self.setupAutoLayout()
        self.setupToolbarItemsEnable()
    }

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        self.setupLoadRequest()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        /*
        self.webView.configuration.userContentController.removeScriptMessageHandler(forName: "copyText")
        self.webView.configuration.userContentController.removeScriptMessageHandler(forName: "cathayWalkerDataUploadRefresh")
        self.webView.configuration.userContentController.removeScriptMessageHandler(forName: "healthAppOpen")
        self.webView.stopLoading()
        */
    }
    
    deinit {}
}

// MARK: - UI Setup
extension MMLWebViewController
{
    fileprivate func setupWKWebView()
    {
        /*
        self.configuration.userContentController.add(self, name: "copyText")
        self.configuration.userContentController.add(self, name: "cathayWalkerDataUploadRefresh")
        self.configuration.userContentController.add(self, name: "healthApp")
        */
        
        self.webView = WKWebView(frame: CGRect.zero, configuration: self.configuration)
        self.webView.navigationDelegate = self
        //self.webView.uiDelegate = self
    }
    
    /// 設置 AutoLayout
    private final func setupAutoLayout()
    {
        self.webView.translatesAutoresizingMaskIntoConstraints = false
        view.insertSubview(self.webView, at: 0)
        
        var topAnchor, bottomAnchor: NSLayoutYAxisAnchor
        
        if #available(iOS 11.0, *)
        {
            topAnchor = self.view.safeAreaLayoutGuide.topAnchor
            bottomAnchor = self.view.safeAreaLayoutGuide.bottomAnchor
        }
        else
        {
            topAnchor = self.topLayoutGuide.bottomAnchor
            bottomAnchor = self.bottomLayoutGuide.topAnchor
        }
        
        NSLayoutConstraint.activate([
            self.webView.topAnchor.constraint(equalTo: topAnchor),
            self.webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            self.webView.bottomAnchor.constraint(equalTo: bottomAnchor),
            self.webView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
    }
    
    /// 設置下方 Toolbar item enable
    private final func setupToolbarItemsEnable()
    {
        backItem.isEnabled = webView.canGoBack
        forwardItem.isEnabled = webView.canGoForward
        actionItem.isEnabled = self.showActionItem ? (webView.url != nil) : false
        if !self.showActionItem {
            actionItem.tintColor = .clear
        }
    }
}
 

// MARK: - Web View Setup
extension MMLWebViewController
{
    
    /// 設置 WebView request
    final func setupLoadRequest()
    {
        title = webInfo?.title ?? "國泰人壽"
        
        let urlString: String = webInfo?.url ?? "https://\(Environment.host)"
        
        guard let url: URL = URL(string: urlString) else
        {
            return setupWebViewRequestFail()
        }

        var request = URLRequest(url: url)

        if url.absoluteString.contains("mobilechat")
        {
            // 線上諮詢, 什麼都不要做, 不然 web 吃不到 Id
        }
        else if url.absoluteString.contains("cathaylife.com") == true
        {
            request.httpShouldHandleCookies = true

            if let cookies = HTTPCookieStorage.shared.cookies
            {
                request.allHTTPHeaderFields = HTTPCookie.requestHeaderFields(with: cookies)

                
            }
        }
        
        webView.load(request)
    }
    
    /// 顯示初始化 WebView request 失敗
    private final func setupWebViewRequestFail()
    {
        /*
        let title: String = "很抱歉"
        let message: String = "發生異常, 無法載入網頁"
        let alert: MMLAlertViewController? = MMLAlertViewController.shared(title: title,
                                                                           message: message,
                                                                           tapToExit: false)
        
        alert?.addButton(SolidButton(title: "確定"))
        { alert in
            
            alert.dismiss(animated: false)
            {
                self.dismiss(animated: true)
            }
        }
        
        showAlert(alert)
        */
    }
}

// MARK: - IBAction
extension MMLWebViewController
{
    /// webView 上一頁
    @IBAction private final func backItemClicked(_: UIBarButtonItem)
    {
        self.webView.goBack()
        
        guard self.shouldReloadWhenGoBackPreviousPage else {return }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
        {
            self.webView.reload()
        }
        
        
        
    }
    
    /// webView 下一頁
    @IBAction private final func forwardItemClicked(_: UIBarButtonItem)
    {
        webView.goForward()
    }
    
    /// webView share
    @IBAction private final func actionItemClicked(_: UIBarButtonItem)
    {
        guard let url: URL = webView.url else { return }
        
        let act: UIActivityViewController = UIActivityViewController(activityItems: [url],
                                                                     applicationActivities: nil)
        
        present(act, animated: true)
    }
    
    /// 離開
    @IBAction private final func exitItemClicked(_: UIBarButtonItem?)
    {
        
        
        //self.dismiss(animated: true, completion: nil)
        
        /*
        if openNPSFromLoginSuccess
        {
            replaceRootViewControllerAfterNPS()
            
            return
        }
        
        // 進入線上諮詢, 離開時要 call 這個 javascript
        if webView.url?.absoluteString.contains("mobilechat") == true
        {
            webView.evaluateJavaScript("closeConnection()")
        }
        
        if shouldReload == true
        {
            dismissSelfAfterUpdateUserInfo()
        }
        else
        {
            dismissSelf()
        }
        */
    }
    
    /**
     填完 NPS 後, 替換 RootViewController.
     
     `這裡的 NPS 應該是從登入後第一次填寫 NPC`
     */
    private final func replaceRootViewControllerAfterNPS()
    {
        
        /*
        guard openNPSFromLoginSuccess else { return }
        
        let story: UIStoryboard = UIStoryboard(name: "TabBarController", bundle: nil)
        
        guard let newRoot: UIViewController = story.instantiateInitialViewController() else { return }
        
        AppDelegate.shared().replaceRootViewController(newRoot)
        */
    }
    
    
    /// 跟新完 UserInfo 後, 關閉自己
    private final func dismissSelfAfterUpdateUserInfo()
    {
        /*
        NotificationCenter.default.post(name: MMLWebViewController.ShouldReload, object: nil)
        
        showHud()
        
        APIResponse<OCI2_0600_QueryUserInfoModel>.fetch(actionName: "OCI2_0600", methodName: "queryUserInfo")
        { callback in
            
            self.hideHud()
            
            if callback.rtnCode == 1
            {
                MemoryTemp.singleton.userInfo = callback.model
            }
            
            self.dismissSelf()
        }
        */
    }
    
    /// 關閉自己
    private final func dismissSelf()
    {
        /*
        // 有時候 webView 會有 alert
        if let presentedViewController: UIViewController = presentedViewController
        {
            presentedViewController.dismiss(animated: false, completion: {
                self.dismiss(animated: true)
            })
        }
        else
        {
            dismiss(animated: true)
        }
        */
    }
}


// MARK: - WKNavigationDelegate

extension MMLWebViewController: WKNavigationDelegate
{
    // 2
    func webView(_: WKWebView, didStartProvisionalNavigation _: WKNavigation!)
    {
        setupToolbarItemsEnable()
        loadingView.isHidden = false
    }

    func webView(_ webView: WKWebView, didFinish _: WKNavigation!)
    {
        
        setupToolbarItemsEnable()
        loadingView.isHidden = true
        /*
        checkShouldReload()
        checkSignInExpired()
        
        if let title = title, title == "Cathay Walker"
        {
            self.actionItem.isEnabled = false
            self.webView.allowsLinkPreview = false
            webView.evaluateJavaScript("document.body.style.webkitTouchCallout='none';")
        }
        */
    }

    func webView(_: WKWebView, didFailProvisionalNavigation _: WKNavigation!, withError _: Error)
    {
        
        setupToolbarItemsEnable()
        loadingView.isHidden = true
 
    }

    func webView(_: WKWebView, didFail _: WKNavigation!, withError _: Error)
    {
        
        setupToolbarItemsEnable()
        loadingView.isHidden = true
 
    }

    // 1
    func webView(_: WKWebView,
                 decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
    {
        
        if  let url = navigationAction.request.url,
            let scheme = url.scheme
        {
            let lastPathComponent = url.lastPathComponent
            
            if scheme == "js2app" && lastPathComponent == "closeRefresh"
            {
                shouldReload = true
                decisionHandler(.cancel)
                exitItemClicked(nil)
                
                return
            }
            else if scheme == "line" ||
                    scheme == "mmo" ||
                    scheme == "itms-apps" ||
                    scheme == "tel"
            {
                if UIApplication.shared.canOpenURL(url)
                {
                    decisionHandler(.cancel)
                    UIApplication.shared.open(url)
                    return
                }
            }
        }

        decisionHandler(.allow)
 
    }

    // 沒實作的話, 我記得有怪現象
    func webView(_: WKWebView, didReceiveServerRedirectForProvisionalNavigation _: WKNavigation!)
    {
        
    }

    
    /**
     檢查是否需要 Reload
     
     條件為目前 WebView 的 URL String 是否包含 shouldReloadList 裡的字串
     */
    private final func checkShouldReload()
    {
        guard let urlString: String = webView.url?.absoluteString else { return }
        
        shouldReload = shouldReloadList.map({ (string) -> Bool in
            urlString.contains(string)
        }).contains(true)
    }
    
    /**
     檢查登入是否過期
     
     如果 webView 的 source code 有包含 `$MobiAppLoginFlag` 字串, 就代表過期了.
     */
    private final func checkSignInExpired()
    {
        /*
        let javaScript: String = "document.documentElement.outerHTML"
        
        webView.evaluateJavaScript(javaScript)
        { html, _ in
            
            guard let html: String = html as? String else { return }
            guard html.contains("$MobiAppLoginFlag") else { return }
            
            AppDelegate.handleAPITimeOut(with: "操作逾時",
                                         message: "您閒置的時間已經超過系統時限，為保障您個人的使用安全，本系統已將您登出。 若您欲繼續使用，請重新登入")
        }
        */
    }
}


// MARK: - WKUIDelegate
/*
extension MMLWebViewController: WKUIDelegate
{
    func webView(_ webView: WKWebView,
                 runJavaScriptAlertPanelWithMessage message: String,
                 initiatedByFrame _: WKFrameInfo,
                 completionHandler: @escaping () -> Void)
    {
        
        // Web Alert handle
        let alert: UIAlertController = UIAlertController(title: webView.title,
                                                         message: message,
                                                         preferredStyle: .alert)

        let ok: UIAlertAction = UIAlertAction(title: "確定", style: .default)

        alert.addAction(ok)
        present(alert, animated: true)
        completionHandler()
        
    }

    func webView(_: WKWebView,
                 createWebViewWith _: WKWebViewConfiguration,
                 for navigationAction: WKNavigationAction,
                 windowFeatures _: WKWindowFeatures) -> WKWebView?
    {
        
        // 當 Web 行為為 open window
        if let url: URL = navigationAction.request.url
        {
            if #available(iOS 10.0, *)
            {
                UIApplication.shared.open(url, options: [:])
            }
            else
            {
                UIApplication.shared.openURL(url)
            }
        }
 
        
        return nil
    }
}
*/

/*
// MARK: - WKScriptMessageHandler

extension MMLWebViewController: WKScriptMessageHandler
{
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage)
    {
        if message.name == "copyText", let msg = message.body as? String {
            let pastboard = UIPasteboard.general
            pastboard.string = msg
        }

        if message.name == "healthApp" {
            self.launchHealthApp()
        }
        
        if message.name == "cathayWalkerDataUploadRefresh" {
            self.updateHealthInfoAndReload()
        }
    }
    
    func launchHealthApp()
    {
        guard let url: URL = URL(string: "x-apple-health://")
            else
        {
            return
        }
        
        
        guard UIApplication.shared.canOpenURL(url)
            else
        {
            return
        }
        
        UIApplication.shared.open(url)
    }
    
    func updateHealthInfoAndReload()
    {
        MMLHealthInfoManager.fetchStepCount(duration: -39) { (isAuthorized, stepData) in
            
            if !isAuthorized {
                // if 未授權 ....
                return
            }
            
            MMLHealthInfoManager.updateHealthInfo(stepData: stepData, completionHandlerAfterUpdate: {
                (updateResult) in
                
                
                if case .otherError(let response) = updateResult
                {
                    self.showAlert(MMLAlertViewController.forAPIResponse(response))
                    return
                }
                
                if case .fail(let responseMessage) = updateResult, responseMessage.count > 0
                {
                    self.showAlert(MMLAlertViewController.shared(title: "Cathay Walker", message: responseMessage))
                    return
                }
                
                guard case .success(let responseMessage) = updateResult, responseMessage.count > 0
                else {
                    return
                }

                
                if #available(iOS 10.0, *)
                {
                    let center = UNUserNotificationCenter.current()
                    center.requestAuthorization(options: [.alert]) { (granted, error) in
                        // ...
                    }
                    center.delegate = UIApplication.shared.delegate as! AppDelegate
                    
                    let content = UNMutableNotificationContent()
                    content.title = "Cathay Walker"
                    content.body = responseMessage // TODO: updateSuccess ? "計步資料已同步！" : "計步資料未同步！"
                    let request = UNNotificationRequest(identifier: "updateHealthInfoSuccess", content: content, trigger: nil)
                    center.add(request)
                }

                self.webView.reload()
            })
        }
    }
}
*/


extension MMLWebViewController
{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let cookiesVC = segue.destination as? CookiesViewController
        cookiesVC?.cookiesText = self.cookiesText
    }
}





