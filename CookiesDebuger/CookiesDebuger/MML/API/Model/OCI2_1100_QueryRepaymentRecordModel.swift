//
//  OCI2_1100_QuertRepaymentRecordModel.swift
//  MyMobileLife
//
//  Created by jill on 2017/6/27.
//  Copyright © 2017年 CathayLife. All rights reserved.
//

import Foundation
import ObjectMapper

struct OCI2_1100_RecordModel: Mappable
{
    private(set) var amount: String = "未知"
    private(set) var status: String = "未知"
    private(set) var date: String = "未知"
    private(set) var mode: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        amount <- map["amount"]
        status <- map["status"]
        date  <- map["date"]
        mode   <- map["mode"]
    }
}

struct OCI2_1100_LoanListModel: Mappable
{
    private(set) var title: String = "未知"
    private(set) var loanRecordList: [OCI2_1100_RecordModel] = []
    private(set) var policyNo: String = "未知"
   
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        title <- map["title"]
        loanRecordList <- map["loanRecordList"]
        policyNo <- map["policyNo"]
    }
}

struct OCI2_1100_RepaymentListModel: Mappable
{
    private(set) var title: String = "未知"
    private(set) var repaymentRecordList: [OCI2_1100_RecordModel] = []
    private(set) var policyNo: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        title <- map["title"]
        repaymentRecordList <- map["repaymentRecordList"]
        policyNo <- map["policyNo"]
    }
}

struct OCI2_1100_LoanRepaymentRecordModel: Mappable
{
    private(set) var amount: String = "未知"
    private(set) var status: String = "未知"
    private(set) var type: Int = 0
    private(set) var date: String = "未知"
    private(set) var mode: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        amount <- map["amount"]
        status   <- map["status"]
        type <- map["type"]
        date   <- map["date"]
        mode   <- map["mode"]
    }
}

struct OCI2_1100_ClosedPayModel: Mappable
{
    private(set) var payDate: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        payDate <- map["payDate"]
    }
}

struct OCI2_1100_SpecialPayListModel: Mappable
{
    private(set) var title: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        title <- map["title"]
    }
}

struct OCI2_1100_SpecialPayModel: Mappable
{
    private(set) var specialPayList: [OCI2_1100_SpecialPayListModel] = []
    private(set) var phone: String = "未知"
    private(set) var name: String = "未知"
    private(set) var addr: String = "未知"

    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        specialPayList <- map["specialPayList"]
        phone <- map["phone"]
        name <- map["name"]
        addr <- map["addr"]
    }
}

struct OCI2_1100_SpecialClientModel: Mappable
{
    private(set) var phone: String = "未知"
    private(set) var specialClientList: [OCI2_1100_SpecialPayListModel] = []
    private(set) var name: String = "未知"
    private(set) var showPhone: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        phone <- map["phone"]
        specialClientList <- map["specialClientList"]
        name <- map["name"]
        showPhone <- map["showPhone"]
    }
}

struct OCI2_1100_LoanRepaymentListModel: Mappable
{
    private(set) var title: String = "未知"
    private(set) var loanAmount: String = "未知"
    private(set) var loanRepaymentListRecordList: [OCI2_1100_LoanRepaymentRecordModel] = []
    private(set) var policyNo: String = "未知"
    private(set) var loanInterest: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        title <- map["title"]
        loanAmount <- map["loanAmount"]
        loanRepaymentListRecordList <- map["loanRepaymentListRecordList"]
        policyNo <- map["policyNo"]
        loanInterest <- map["loanInterest"]
    }
}

struct OCI2_1100_RepaymentRecordModel: Mappable
{
    private(set) var loanList: [OCI2_1100_LoanListModel] = []
    private(set) var repaymentList: [OCI2_1100_RepaymentListModel] = []
    private(set) var loanRepaymentList: [OCI2_1100_LoanRepaymentListModel] = []
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        loanList <- map["loanList"]
        repaymentList <- map["repaymentList"]
        loanRepaymentList <- map["loanRepaymentList"]
    }
}

struct OCI2_1100_QueryRepaymentRecordModel: Mappable
{
    private(set) var queryRepaymentRecordModel: OCI2_1100_RepaymentRecordModel?
    private(set) var claimsScheduleCnt: Int = 0
    private(set) var claimsSchedule: OCI2_0800_QueryClaimsSchedule?

    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        queryRepaymentRecordModel <- map["queryRepaymentRecordModel"]
        claimsScheduleCnt <- map["claimsScheduleCnt"]
        claimsSchedule <- map["claimsSchedule"]
    }
}
