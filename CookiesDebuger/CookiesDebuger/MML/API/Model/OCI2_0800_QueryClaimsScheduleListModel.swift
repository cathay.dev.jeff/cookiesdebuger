//
//  OCI2_0800_QueryClaimsScheduleListModel.swift
//  MyMobileLife
//
//  Created by JackyChen on 2017/7/12.
//  Copyright © 2017年 CathayLife. All rights reserved.
//

import Foundation
import ObjectMapper

// REMARK: - SpecialPayList

struct OCI2_0800_SpecialPayModel: Mappable
{
    private(set) var phone: String = "未知"
    private(set) var specialPayArray: [String] = []
    private(set) var name: String = "未知"
    private(set) var addr: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        phone <- map["phone"]
        specialPayArray <- map["specialPayArray"]
        name  <- map["name"]
        addr <- map["addr"]
    }
}

struct OCI2_0800_SpecialPayListModel: Mappable
{
    private(set) var payAmount: String = "未知"
    private(set) var apcID: String = "未知"
    private(set) var applicationDate: String = "未知"
    private(set) var acceptNo: String = "未知"
    private(set) var type: Int = 0
    private(set) var specialPayModel: OCI2_0800_SpecialPayModel?
    private(set) var apcName: String = "未知"
    private(set) var accidentDate: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        payAmount <- map["payAmount"]
        apcID <- map["apcID"]
        applicationDate <- map["applicationDate"]
        acceptNo <- map["acceptNo"]
        type <- map["type"]
        specialPayModel <- map["specialPayModel"]
        apcName <- map["apcName"]
        accidentDate <- map["accidentDate"]
    }
}

// REMARK: - SpecialClientList

struct OCI2_0800_SpecialClientModel: Mappable
{
    private(set) var phone: String = "未知"
    private(set) var name: String = "未知"
    private(set) var specialClientArray: [String] = []
    private(set) var showPhone: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        phone <- map["phone"]
        name  <- map["name"]
        specialClientArray <- map["specialClientArray"]
        showPhone <- map["showPhone"]
    }
}

struct OCI2_0800_SpecialClientListModel: Mappable
{
    private(set) var payAmount: String = "未知"
    private(set) var applicationDate: String = "未知"
    private(set) var acceptNo: String = "未知"
    private(set) var type: Int = 0
    private(set) var specialClientModel: OCI2_0800_SpecialClientModel?
    private(set) var apcName: String = "未知"
    private(set) var accidentDate: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        payAmount <- map["payAmount"]
        applicationDate <- map["applicationDate"]
        acceptNo <- map["acceptNo"]
        type <- map["type"]
        specialClientModel <- map["specialClientModel"]
        apcName <- map["apcName"]
        accidentDate <- map["accidentDate"]
    }
}

// REMARK: - ClosedNoPayList

struct OCI2_0800_ClosedNoPayListModel: Mappable
{
    private(set) var payAmount: String = "未知"
    private(set) var applicationDate: String = "未知"
    private(set) var acceptNo: String = "未知"
    private(set) var type: Int = 0
    private(set) var apcName: String = "未知"
    private(set) var accidentDate: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        payAmount <- map["payAmount"]
        applicationDate <- map["applicationDate"]
        acceptNo <- map["acceptNo"]
        type <- map["type"]
        apcName <- map["apcName"]
        accidentDate <- map["accidentDate"]
    }
}

// REMARK: - ClosedNoPayList

struct OCI2_0800_InvestigateListModel: Mappable
{
    private(set) var payAmount: String = "未知"
    private(set) var applicationDate: String = "未知"
    private(set) var acceptNo: String = "未知"
    private(set) var type: Int = 0
    private(set) var apcName: String = "未知"
    private(set) var accidentDate: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        payAmount <- map["payAmount"]
        applicationDate <- map["applicationDate"]
        acceptNo <- map["acceptNo"]
        type <- map["type"]
        apcName <- map["apcName"]
        accidentDate <- map["accidentDate"]
    }
}

// REMARK: - ClosedPayList

struct OCI2_0800_InsuranceListModel: Mappable
{
    private(set) var amount: String = "未知"
    private(set) var title: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        amount <- map["amount"]
        title  <- map["title"]
    }
}

struct OCI2_0800_ItemList_Model: Mappable
{
    private(set) var title: String = "未知"
    private(set) var insuranceList: [OCI2_0800_InsuranceListModel] = []
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        title <- map["title"]
        insuranceList  <- map["insuranceList"]
    }
}

struct OCI2_0800_ClosedPayList_2_Model: Mappable
{
    private(set) var title: String = "未知"
    private(set) var benefitratio: String = "未知"
    private(set) var policyNo: String = "未知"
    private(set) var totalPayAmount: String = "未知"
    private(set) var itemList: [OCI2_0800_ItemList_Model] = []
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        title <- map["title"]
        benefitratio  <- map["benefitratio"]
        itemList  <- map["itemList"]
        policyNo  <- map["policyNo"]
        totalPayAmount <- map["totalPayAmount"]
        itemList  <- map["itemList"]
    }
}

struct OCI2_0800_ClosedPayModel: Mappable
{
    private(set) var payDate: String = "未知"
    private(set) var closedPayList: [OCI2_0800_ClosedPayList_2_Model] = []
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        payDate <- map["payDate"]
        closedPayList  <- map["closedPayList"]
    }
}

struct OCI2_0800_ClosedPayListModel: Mappable
{
    private(set) var payAmount: String = "未知"
    private(set) var applicationDate: String = "未知"
    private(set) var acceptNo: String = "未知"
    private(set) var type: Int = 0
    private(set) var closedPayModel: OCI2_0800_ClosedPayModel?
    private(set) var specialClientModel: OCI2_0800_SpecialClientModel?
    private(set) var apcName: String = "未知"
    private(set) var accidentDate: String = "未知"
    
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        payAmount <- map["payAmount"]
        applicationDate <- map["applicationDate"]
        acceptNo <- map["acceptNo"]
        type <- map["type"]
        closedPayModel <- map["closedPayModel"]
        specialClientModel <- map["specialClientModel"]
        apcName <- map["apcName"]
        accidentDate <- map["accidentDate"]
    }
}

// REMARK: - SpecialClientList

struct OCI2_0800_QueryClaimsScheduleListModel: Mappable
{
    var claimsSchedule: OCI2_0800_QueryClaimsSchedule?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        claimsSchedule <- map["claimsSchedule"]
    }
}

struct OCI2_0800_QueryClaimsSchedule: Mappable
{
    private(set) var specialPayList: [OCI2_0800_SpecialPayListModel] = []
    private(set) var specialClientList: [OCI2_0800_SpecialClientListModel] = []
    private(set) var closedNoPayList: [OCI2_0800_ClosedNoPayListModel] = []
    private(set) var investigateList: [OCI2_0800_InvestigateListModel] = []
    private(set) var closedPayList: [OCI2_0800_ClosedPayListModel] = []
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        specialPayList <- map["specialPayList"]
        specialClientList <- map["specialClientList"]
        closedNoPayList <- map["closedNoPayList"]
        investigateList <- map["investigateList"]
        closedPayList <- map["closedPayList"]
    }
}
