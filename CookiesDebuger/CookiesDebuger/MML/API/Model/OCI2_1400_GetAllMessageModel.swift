//
//  OCI2_0800_QueryClaimsShouldReadyFileListModel.swift
//  MyMobileLife
//
//  Created by jill on 2017/6/27.
//  Copyright © 2017年 CathayLife. All rights reserved.
//

import Foundation
import ObjectMapper

struct OCI2_1400_GetAllMessageModel: Mappable
{
    var PersonalInfo: [OCI2_1400_MessageInfo] = []
    var BenefitInfo: [OCI2_1400_MessageInfo] = []
    private(set) var CNT_NR: Int = 0

    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        PersonalInfo <- map["PersonalInfo"]
        BenefitInfo <- map["BenefitInfo"]
        CNT_NR <- map["CNT_NR"]
    }
}

struct OCI2_1400_MessageInfo: Mappable
{
    private(set) var ID: Int = 0
    private(set) var TITLE: String = ""
    private(set) var CONT: String = ""
    private(set) var BTN_URL: String = ""
    private(set) var IMG_URL: String = ""
    private(set) var NOTI_DATE: String = ""
    var IS_READ: String = ""

    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        ID <- map["ID"]
        TITLE <- map["TITLE"]
        CONT <- map["CONT"]
        BTN_URL <- map["BTN_URL"]
        IMG_URL <- map["IMG_URL"]
        NOTI_DATE <- map["NOTI_DATE"]
        IS_READ <- map["IS_READ"]
    }
}
