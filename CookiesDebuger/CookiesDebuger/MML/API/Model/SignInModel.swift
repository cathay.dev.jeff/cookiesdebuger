//
//  SignInModel.swift
//  MyMobileLife
//
//  Created by dev1 on 2017/7/31.
//  Copyright © 2017年 CathayLife. All rights reserved.
//

import Foundation
import ObjectMapper

struct SignInModel: Mappable
{
    private(set) var PWD: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        PWD <- map["PWD"]
    }
}
