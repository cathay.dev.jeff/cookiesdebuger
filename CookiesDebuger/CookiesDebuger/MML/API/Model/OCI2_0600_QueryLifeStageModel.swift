//
//  OCI2_0600_QueryLifeStageModel.swift
//  MyMobileLife
//
//  Created by jill on 2017/6/27.
//  Copyright © 2017年 CathayLife. All rights reserved.
//

import Foundation
import ObjectMapper


struct OCI2_0600_QueryLifeStageModel: Mappable
{
    private(set) var lifeStage: String = ""
    private(set) var prodStatus: [OCI2_0600_ProdStatusModel] = []
    private(set) var neededProd: String = ""
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        lifeStage <- map["lifeStage"]
        prodStatus <- map["prodStatus"]
        neededProd <- map["neededProd"]
        
    }
}

struct OCI2_0600_ProdStatusModel: Mappable
{
    private(set) var IS_EXIST: String = ""
    private(set) var DESC2: String = ""
    private(set) var DESC1: String = ""
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        IS_EXIST <- map["IS_EXIST"]
        DESC2 <- map["DESC2"]
        DESC1 <- map["DESC1"]
    }
}
