//
//  OCI2_0500_QueryCalendarDataModel.swift
//  MyMobileLife
//
//  Created by dev1 on 2017/7/3.
//  Copyright © 2017年 CathayLife. All rights reserved.
//

import Foundation
import ObjectMapper

struct OCI2_0500_AnnuityReceiveListModel: Mappable
{
    private(set) var payee: String?
    private(set) var receiveDate: String?
    private(set) var receiveAmount: String?
    private(set) var receiveMode: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        payee <- map["payee"]
        receiveDate <- map["receiveDate"]
        receiveAmount <- map["receiveAmount"]
        receiveMode <- map["receiveMode"]
    }
}

struct OCI2_0500_AnnuityInformationModel: Mappable
{
    private(set) var totalReceiveAmount: String?
    private(set) var totalReceiveAmount_NTD: String?
    private(set) var trnKind: String?
    private(set) var isReceive: Bool?
    private(set) var annuityReceiveList: [OCI2_0500_AnnuityReceiveListModel]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        totalReceiveAmount <- map["totalReceiveAmount"]
        totalReceiveAmount_NTD <- map["totalReceiveAmount_NTD"]
        trnKind <- map["trnKind"]
        isReceive <- map["isReceive"]
        annuityReceiveList <- map["annuityReceiveList"]
    }
}

struct OCI2_0500_PaymentModel: Mappable
{
    private(set) var payInfo: [String: String]?
    private(set) var shouldPayAmount: String?
    private(set) var shouldPayAmount_NTD: String?
    private(set) var isShowPayButton: Bool?
    private(set) var paidPeriod: String?
    private(set) var payPeriod: String?
    private(set) var nextShouldPayDate: String?
    private(set) var payPipeline: String?
    private(set) var payAccount: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        payInfo <- map["payInfo"]
        shouldPayAmount <- map["shouldPayAmount"]
        shouldPayAmount_NTD <- map["shouldPayAmount_NTD"]
        isShowPayButton <- map["isShowPayButton"]
        paidPeriod <- map["paidPeriod"]
        payPeriod <- map["payPeriod"]
        nextShouldPayDate <- map["nextShouldPayDate"]
        payPipeline <- map["payPipeline"]
        payAccount <- map["payAccount"]
    }
}

struct OCI2_0500_DetailListModel: Mappable
{
    private(set) var tdate: String?
    private(set) var day: String?
    private(set) var week: String?
    private(set) var shouldPayAmount: String?
    private(set) var shouldReceiveAmount: String?
    private(set) var policyNo: String?
    private(set) var policyTitle: String?
    private(set) var abnormalCharge: Int?
    private(set) var paymentModel: OCI2_0500_PaymentModel?
    private(set) var annuityInformationList: [OCI2_0500_AnnuityInformationModel]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        tdate <- map["tdate"]
        day <- map["day"]
        week <- map["week"]
        shouldPayAmount <- map["shouldPayAmount"]
        shouldReceiveAmount <- map["shouldReceiveAmount"]
        policyNo <- map["policyNo"]
        policyTitle <- map["policyTitle"]
        abnormalCharge <- map["abnormalCharge"]
        paymentModel <- map["paymentModel"]
        annuityInformationList <- map["annuityInformationList"]
    }
}

struct OCI2_0500_CalendarListModel: Mappable
{
    private(set) var yearMonth: String?
    private(set) var year: String?
    private(set) var month: String?
    private(set) var payAmount: String?
    private(set) var shouldReceiveAmount: String?
    private(set) var detailList: [OCI2_0500_DetailListModel]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        yearMonth <- map["yearMonth"]
        year <- map["year"]
        month <- map["month"]
        payAmount <- map["payAmount"]
        shouldReceiveAmount <- map["shouldReceiveAmount"]
        detailList <- map["detailList"]
    }
}

struct OCI2_0500_QueryCalendarDataModel: Mappable
{
    private(set) var startYearMonth: String?
    private(set) var endYearMonth: String?
    private(set) var totalReceiveAmount: String?
    private(set) var totalPayAmount: String?
    private(set) var isShowPolicyCalendarModel: Bool?
    private(set) var calendarList:[OCI2_0500_CalendarListModel]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        startYearMonth <- map["startYearMonth"]
        endYearMonth <- map["endYearMonth"]
        totalReceiveAmount <- map["totalReceiveAmount"]
        isShowPolicyCalendarModel <- map["policyCalendarModel.isShowPolicyCalendarModel"]
        calendarList <- map["policyCalendarModel.calendarList"]
    }
}
