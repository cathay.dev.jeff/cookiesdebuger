//
//  OCI2_0700_QuerySuitInsuranceListModel.swift
//  MyMobileLife
//
//  Created by Shinren Pan on 2017/7/1.
//  Copyright © 2017年 CathayLife. All rights reserved.
//

import Foundation
import ObjectMapper

struct OCI2_0700_QuerySuitInsuranceListModel: Mappable
{
    private(set) var code: String = "未知"
    private(set) var title: String = "未知"
    private(set) var content: String = "未知"
    private(set) var imageUrl: String = "未知"
    private(set) var detailUrl: String = "未知"
    private(set) var isShowReservationAdvisoryButton: Bool = false
    private(set) var reservationAdvisoryUrl: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        code <- map["code"]
        title <- map["title"]
        content <- map["content"]
        imageUrl <- map["imageUrl"]
        detailUrl <- map["detailUrl"]
        isShowReservationAdvisoryButton <- map["isShowReservationAdvisoryButton"]
        reservationAdvisoryUrl <- map["reservationAdvisoryUrl"]
    }
}
