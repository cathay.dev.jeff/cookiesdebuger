//
//  OCI2_0400_QueryInvestPolicyPerformanceModel.swift
//  MyMobileLife
//
//  Created by dev1 on 2017/6/27.
//  Copyright © 2017年 CathayLife. All rights reserved.
//

import Foundation
import ObjectMapper

struct OCI2_0400_SubjectListModel: Mappable
{
    private(set) var title: String = "未知"
    private(set) var rate: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        title <- map["title"]
        rate <- map["rate"]
    }
}

struct OCI2_0400_MyInvestPolicyListModel: Mappable
{
    private(set) var title: String = "未知"
    private(set) var policyNo: String = "未知"
    private(set) var subjectList: [OCI2_0400_SubjectListModel] = []
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        title <- map["title"]
        policyNo <- map["policyNo"]
        subjectList <- map["subjectList"]
    }
}

struct OCI2_0400_CashRallySubListModel: Mappable
{
    private(set) var cashRallyDate: String = "未知"
    private(set) var cashRallyAmount: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        cashRallyDate <- map["cashRallyDate"]
        cashRallyAmount <- map["cashRallyAmount"]
    }
}

struct OCI2_0400_CashRallyListModel: Mappable
{
    private(set) var title: String = "未知"
    private(set) var payee: String = "未知"
    private(set) var policyNo: String = "未知"
    private(set) var cashRallySubList: [OCI2_0400_CashRallySubListModel] = []
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        title <- map["title"]
        payee <- map["payee"]
        policyNo <- map["policyNo"]
        cashRallySubList <- map["cashRallySubList"]
    }
}

struct OCI2_0400_CashRallyModel: Mappable
{
    private(set) var curr: String = "未知"
    private(set) var amount: String = "未知"
    private(set) var cashRallyList: [OCI2_0400_CashRallyListModel] = []
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        curr <- map["curr"]
        amount <- map["amount"]
        cashRallyList <- map["cashRallyList"]
    }
}

struct OCI2_0400_PolicyListModel: Mappable
{
    private(set) var title: String?
    private(set) var policyNo: String?
    private(set) var canStillBorrowAmount: String?
    private(set) var loanInterestRate: String?
    private(set) var borrowedAmount: String?
    private(set) var borrowedInterest: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        title <- map["title"]
        policyNo <- map["policyNo"]
        canStillBorrowAmount <- map["canStillBorrowAmount"]
        loanInterestRate <- map["loanInterestRate"]
        borrowedAmount <- map["borrowedAmount"]
        borrowedInterest <- map["borrowedInterest"]
    }
}

struct OCI2_0400_CurrListModel: Mappable
{
    private(set) var policys: String?
    private(set) var curr: String?
    private(set) var totalBorrowAmount: String?
    private(set) var alreadyLoanAmount: String?
    private(set) var alreadyLoanInterest: String?
    private(set) var policyList: [OCI2_0400_PolicyListModel]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        policys <- map["policys"]
        curr <- map["curr"]
        totalBorrowAmount <- map["totalBorrowAmount"]
        alreadyLoanAmount <- map["alreadyLoanAmount"]
        alreadyLoanInterest <- map["alreadyLoanInterest"]
        policyList <- map["policyList"]
    }
}

struct OCI2_0400_CanStillLoanAmountModel: Mappable
{
    private(set) var policyTotalBorrowCurr: String?
    private(set) var policyTotalBorrowAmount: String?
    private(set) var currList: [OCI2_0400_CurrListModel]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        policyTotalBorrowCurr <- map["policyTotalBorrowCurr"]
        policyTotalBorrowAmount <- map["policyTotalBorrowAmount"]
        currList <- map["currList"]
    }
}

struct OCI2_0400_LoanBanners: Mappable
{
    private(set) var content: String?
    private(set) var topTitle: String?
    private(set) var leftTitle: String?
    private(set) var url: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        content <- map["content"]
        topTitle <- map["topTitle"]
        leftTitle <- map["leftTitle"]
        url <- map["url"]
    }
}

struct OCI2_0400_QueryInvestPolicyPerformanceModel: Mappable
{
    private(set) var canStillLoanAmount: OCI2_0400_CanStillLoanAmountModel?
    private(set) var cashRally: OCI2_0400_CashRallyModel?
    private(set) var myInvestPolicyList: [OCI2_0400_MyInvestPolicyListModel] = []
    
    private(set) var loanBanners: [OCI2_0400_LoanBanners] = []

    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        canStillLoanAmount <- map["investPolicyPerformanceModel.canStillLoanAmount"]
        cashRally <- map["investPolicyPerformanceModel.cashRally"]
        myInvestPolicyList <- map["investPolicyPerformanceModel.myInvestPolicyList"]
        
        loanBanners <- map["loanBanners"]
    }
}
