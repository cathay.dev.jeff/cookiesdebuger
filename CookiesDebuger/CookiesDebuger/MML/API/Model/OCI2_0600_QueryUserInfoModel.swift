//
//  OCI2_0600_QueryUserInfoModel.swift
//  MyMobileLife
//
//  Created by dev1 on 2017/6/26.
//  Copyright © 2017年 CathayLife. All rights reserved.
//

import Foundation
import ObjectMapper

struct MemberCardModel: Mappable
{
    enum CardType
    {
        case vip, mySelf, relation, apply
    }
    
    var type: CardType = .relation
    var vipLevel: String?
    var relationshipId: String?
    var relationshipName: String?
    
    var date: String = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
        let result: String = dateFormatter.string(from: Date())
        
        return result
    }()
    
    init(){}
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        relationshipId <- map["relationshipId"]
        relationshipName <- map["relationshipName"]
    }
}

struct OCI2_0600_BannerManagementModel: Mappable
{
    private(set) var title: String = "未知"
    private(set) var order: String = "0"
    private(set) var file: String = "未知"
    private(set) var url: String = "未知"
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        title <- map["title"]
        order <- map["order"]
        file  <- map["file"]
        url   <- map["url"]
    }
}

struct OCI2_0600_QueryUserInfoModel: Mappable
{
    var indexCampaign: [String: String]?
    //var campaign: [String: String]?
    private(set) var isNeedDoNPS: String?
    private(set) var npsUrl: String?
    private(set) var nationality: String = "TW"
    private(set) var isCathayWalker: String = ""
    private(set) var mail: String = ""
    private(set) var sex: Int = 2
    private(set) var isGuarantorContainTravel: String = "N"
    private(set) var isEasyCall: String = "N"
    private(set) var isStudGroupInsrPeriod: Bool = false
    private(set) var isTrnPwdChange: String = "N"
    private(set) var name: String = ""
    private(set) var isTrn: String = "N"
    private(set) var isPolicyVerify: String = "N"
    private(set) var isClaimsVerify: String = "N"
    private(set) var policyVerifyUrl: String = ""
    private(set) var vipLevel: String = "0"
    private(set) var isNetinsr: String = ""
    private(set) var isGuarantor: String = "N"
    private(set) var bannerManagementList: [OCI2_0600_BannerManagementModel] = []
    private(set) var mobile: String = ""
    private(set) var vipServices: [[String: Any]] = []
    private var memberCards: [MemberCardModel] = []
    private(set) var cathayWalkerURL: String = ""
    private(set) var cathayWalkerImgURL: String = ""

    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        indexCampaign <- map["indexCampaign"]
        isNeedDoNPS <- map["isNeedDoNPS"]
        npsUrl <- map["npsUrl"]
        nationality <- map["nationality"]
        isCathayWalker <- map["isCathayWalker"]
        mail <- map["mail"]
        sex <- map["sex"]
        isGuarantorContainTravel <- map["isGuarantorContainTravel"]
        isEasyCall <- map["isEasyCall"]
        isStudGroupInsrPeriod <- map["isStudGroupInsrPeriod"]
        isTrnPwdChange <- map["isTrnPwdChange"]
        name <- map["name"]
        isTrn <- map["isTrn"]
        isPolicyVerify <- map["isPolicyVerify"]
        isClaimsVerify <- map["isClaimsVerify"]
        policyVerifyUrl <- map["policyVerifyUrl"]
        vipLevel <- map["vipLevel"]
        isNetinsr <- map["isNetinsr"]
        isGuarantor <- map["isGuarantor"]
        bannerManagementList <- map["bannerManagementList"]
        mobile <- map["mobile"]
        vipServices <- map["vipServices"]
        memberCards <- map["RelationShipList"]
        cathayWalkerURL <- map["cathayWalkerURL"]
        cathayWalkerImgURL <- map["cathayWalkerImgURL"]

    }
    
    mutating func memberCardList() -> [MemberCardModel]
    {
        guard let userId = MemoryTemp.singleton.userId
        else
        {
            return []
        }
        
        if memberCards.first?.type == .vip
        {
            return memberCards
        }
        
        if memberCards.first?.type == .mySelf
        {
            return memberCards
        }
        
        // 自己的卡
        var selfCard: MemberCardModel = MemberCardModel()
        selfCard.type = .mySelf
        selfCard.relationshipName = name
        selfCard.relationshipId = userId
            
        memberCards.insert(selfCard, at: 0)
        
        // 如果是 VIP
        if let vip: Int = Int(self.vipLevel), vip > 0
        {
            var vipCard: MemberCardModel = MemberCardModel()
            vipCard.type = .vip
            vipCard.relationshipName = name
            
            vipCard.vipLevel = {
                switch vip
                {
                    case 5:
                        return "黃金級"
                    
                    case 6:
                        return "白金級"
                    
                    case 7:
                        return "鑽石級"
                    
                    default:
                        return nil
                }
            }()
            
            memberCards.insert(vipCard, at: 0)
        }
        
        // 新增卡
        var applyCard: MemberCardModel = MemberCardModel()
        applyCard.type = .apply
        
        memberCards.append(applyCard)
        
        return memberCards
    }
    
    mutating func addMemberCard(_ card: MemberCardModel)
    {
        guard let userId: String = MemoryTemp.singleton.userId
        else
        {
            return
        }
        
        if card.relationshipId == userId
        {
            return
        }
        
        let lastIndex: Int = memberCards.count - 1
        
        
        memberCards.insert(card, at: lastIndex)
    }
    
    mutating func deleteMemberCard(_ card: MemberCardModel)
    {
        guard let userId: String = MemoryTemp.singleton.userId
        else
        {
            return
        }
        
        if card.relationshipId == userId
        {
            return
        }
        
        memberCards = memberCards.filter
        { (_card: MemberCardModel) in
            
            return _card.relationshipId != card.relationshipId
        }
    }
}
