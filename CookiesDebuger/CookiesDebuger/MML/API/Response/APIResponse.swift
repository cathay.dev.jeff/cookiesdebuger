//
//  APIResponse.swift
//  MyMobileLife
//
//  Created by dev1 on 2017/6/23.
//  Copyright © 2017年 CathayLife. All rights reserved.
//

import UIKit
import Foundation
import ObjectMapper
import CoreTelephony

final class APIResponse<T: Mappable>: Mappable {
    
    final fileprivate(set) var fetching: Bool = false
    final fileprivate(set) var rtnCode: Int = -999
    final fileprivate(set) var rtnMsg: String = "系統忙碌中"
    final fileprivate(set) var rtnMsgTitle: String = "很抱歉"
    final fileprivate(set) var model: T?
    final fileprivate(set) var models: [T]?
    
    init(){}
    
    init?(map: Map) {}
    
    func mapping(map: Map) {
        
        rtnCode <- map["result.rtnCode"]
        rtnMsg  <- map["result.rtnMsg"]
        model   <- map["result.data"]
        models  <- map["result.datas"]
        rtnMsgTitle <- map["result.rtnMsgTitle"]
    }
}


// MARK: - Class function
extension APIResponse {
    
    final class func fetch(
        actionName: String,
        methodName: String,
        params: [String: Any]? = nil,
        callback: @escaping ((APIResponse) -> Void)) {
        
        var urlComponets = Environment.webservice
        let action = URLQueryItem(name: "ACTION_NAME", value: actionName)
        let method = URLQueryItem(name: "METHOD_NAME", value: methodName)
        urlComponets.queryItems = [action, method]

        guard let url = urlComponets.url else
        {
            fatalError()
        }

        APIManager.request(url, method: .post, parameters: params).responseJSON { (response) in

            
            let result: APIResponse = self.init()

            if let error = response.error as? URLError {

                result.rtnCode = error.code.rawValue

                switch error {

                case URLError.notConnectedToInternet:
                    result.rtnMsg = "無法連線網路, 請檢查您的網路連線"

                case URLError.timedOut:
                    result.rtnMsg = "連線逾時"

                default:
                    result.rtnMsg = "連線似乎發生了點問題，建議您切換網路環境或稍後再試哦！"
                }
//
//                CathayUserLogger.tagEvent(
//                    name: "連線異常",
//                    attributes: [
//                        "Action": actionName,
//                        "Method": methodName,
//                        "errorCode": String(error.errorCode)
//                    ]
//                )

                return callback(result)
            }

            guard let json = response.result.value as? [String: Any] else {

                return callback(result)
            }

            let map: Map = Map(mappingType: .fromJSON, JSON: json)

            result.mapping(map: map)

            /*
            switch result.rtnCode {

            case -100:
                AppDelegate.handleAPITimeOut(with: result.rtnMsgTitle, message: result.rtnMsg)

            default:
                callback(result)
            }
            */
            callback(result)
        }
    }
}



// MARK: - Public
extension APIResponse {
    
    final func fetch(
        actionName: String,
        methodName: String,
        params: [String: Any]? = nil,
        callback: (() -> Void)? = nil) {
        
//        if fetching == true {
//
//            return
//        }
//
//        fetching = true
//
//        let me = type(of: self)
//
//        me.fetch(actionName: actionName, methodName: methodName, params: params) { [weak self] (result) in
//
//            guard let `self` = self else {
//
//                return
//            }
//
//            self.rtnMsgTitle = result.rtnMsgTitle
//            self.rtnMsg = result.rtnMsg
//            self.model = result.model
//            self.models = result.models
//            self.fetching = false
//            self.rtnCode = result.rtnCode
//
//            if let callback = callback {
//
//                callback()
//            }
//        }
    }
}


extension APIResponse {
    
    final func cleanData() {
        
        model = nil
        models = nil
    }
}


/*
["DEVICE_INFO": "4.3.10\\12.0\\x86_64\\未知電信營運商", "channel": "MML", "type": "0", "username": "T18982746E", "password": "q1234567"]
["type": "0", "channel": "MML", "password": "q1234567", "username": "T18982746E", "DEVICE_INFO": "4.3.10\\12.0\\x86_64\\未知電信營運商"]
*/

// MARK: - 登入
extension APIResponse {
    
    final class func signIn(_ params: [String: String], callback: @escaping ((APIResponse) -> Void)) {
        
        // User 需求: 登入 API 需要帶 app版本, os版本, device機型, 電信營運商, 並用 \ 隔開
        var params = params
        let appVersion = "4.3.10"//Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let osVersion = "12.0"//UIDevice.current.systemVersion
        let deviceType = UIDevice.deviceType()
        let networkInfo = CTTelephonyNetworkInfo()
        let carrierName = networkInfo.subscriberCellularProvider?.carrierName ?? "未知電信營運商"
        params["DEVICE_INFO"] = appVersion + "\\" + osVersion + "\\" + deviceType + "\\" + carrierName
        

        
        //print(params)
        
        let signInURL = Environment.signInURL
        print(signInURL)
        
        
        APIManager.request(signInURL, method: .post, parameters: params).responseJSON { (response) in
            
            //print("\(response)")
            
            let result: APIResponse = self.init()
            
            if let error = response.error as? URLError {
                
                result.rtnCode = error.code.rawValue
                
                switch error {
                    
                case URLError.notConnectedToInternet:
                    result.rtnMsg = "無法連線網路, 請檢查您的網路連線"
                    
                case URLError.timedOut:
                    result.rtnMsg = "連線逾時"
                    
                default:
                    result.rtnMsg = "連線似乎發生了點問題，建議您切換網路環境或稍後再試哦！"
                }
                
                return callback(result)
            }
            
            // 例外處理, 有時會登入只會回傳 html 網頁, 請爬網頁
            if let error = response.error, let data = response.data, let html = String(data: data, encoding: .utf8) {
                
                result.rtnCode = (error as NSError).code
                
                if html.contains("0x13212072") {
                    
                    result.rtnMsg = "您所輸入的密碼可能已被鎖定！建議您可以到忘記密碼進行補發密碼。"
                }
                
                // else if html.contains("0x13212072") { }
                
                return callback(result);
            }
            
            guard let json = response.result.value as? [String: Any] else {
                
                return callback(result)
            }
            
            let map: Map = Map(mappingType: .fromJSON, JSON: json)
            
            result.mapping(map: map)
            
            /*
            if [1, -2].contains(result.rtnCode) == false {
                
                return callback(result)
            }
            */
            
            callback(result)
            
            /*
            APIResponse<OCI2_0600_QueryUserInfoModel>.fetch(actionName: "OCI2_0600", methodName: "queryUserInfo") { (userInfo) in
                
                MemoryTemp.singleton.userInfo = userInfo.model
                
                if result.rtnCode == -2 && userInfo.rtnCode == 1 {
                    
                }
                else {
                    
                    result.rtnCode = userInfo.rtnCode
                    result.rtnMsg = userInfo.rtnMsg
                }
                
                callback(result)
            }
            */
        }
    }
}


/*
// MARK: - 登出
extension APIResponse {
    
    final class func signOut() {
        
        let signOutURL = Environment.signOutURL
        let params = ["channel": "MML"]
        
        APIManager.request(signOutURL, method: .post, parameters: params)
    }
}
*/
