//
//  Copyright (c) 2017年 shinren.pan@gmail.com All rights reserved.
//


import Alamofire

class CustomServerTrustPolicyManager: ServerTrustPolicyManager {
    
    override func serverTrustPolicy(forHost host: String) -> ServerTrustPolicy? {
        
        if let policy = super.serverTrustPolicy(forHost: host) {
            
            return policy
        }
        else {
            
            return ServerTrustPolicy.customEvaluation { (_, _) in
                
                return false
            }
        }
    }
}

struct APIManager {
    
    private static let singleton: Alamofire.SessionManager = {
        
        let headers: HTTPHeaders = ["X-Requested-With": "XMLHttpRequest"]
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 120.0
        configuration.httpAdditionalHeaders = headers
        configuration.httpCookieStorage = HTTPCookieStorage.shared
        configuration.httpCookieAcceptPolicy = .always
        configuration.httpShouldSetCookies = true
        
        let policies: [String: ServerTrustPolicy] = [
            
            Environment.host.absoluteString: ServerTrustPolicy.pinCertificates(
                certificates: ServerTrustPolicy.certificates(),
                validateCertificateChain: true,
                validateHost: true
            ),
            "campaigns.cathaylife.com.tw": ServerTrustPolicy.pinCertificates(
                certificates: ServerTrustPolicy.certificates(),
                validateCertificateChain: true,
                validateHost: true
            )
        ]
        
        let policyManager = CustomServerTrustPolicyManager(policies: policies)
        
        return SessionManager(configuration: configuration, serverTrustPolicyManager: policyManager)
    }()
    
    @discardableResult static func request(
        _ url: URLConvertible,
        method: Alamofire.HTTPMethod = .get,
        parameters: Parameters? = nil) -> DataRequest {
        
        return self.singleton.request(url, method: method, parameters: parameters)
    }
}
