//
//  MemoryTemp.swift
//  MyMobileLife
//
//  Created by dev1 on 2017/6/23.
//  Copyright © 2017年 CathayLife. All rights reserved.
//

import Foundation
import KeychainAccess

final class MemoryTemp
{
    static let singleton: MemoryTemp = MemoryTemp()

    /*
    var K: String?
    var hybridStatus: [OCI2_0300_HybridStatusModel]?
    var takenNumbers: [(centerName: String, number: String, type: String)] = []
    */
    
    var userId: String?
    /*
    //var deviceToken: String = ""
    var errTimes: Int = 0
    var userInfo: OCI2_0600_QueryUserInfoModel?
    var claimsSchedule: OCI2_0800_QueryClaimsScheduleListModel?
    fileprivate(set) var guaranteeModels: [OCI2_0700_QueryGuaranteeStatusListModel]?
    */
    
    private init(){}
}

/*
extension MemoryTemp
{
    final func findHybridInfo(by id: String, callback: @escaping (OCI2_0300_HybridStatusModel) -> Void)
    {
        if let find = hybridStatus?.first(where: { $0.progId == id} )
        {
            return callback(find)
        }
        
        let params = ["platform": "ios"]
        APIResponse<OCI2_0300_HybridStatusModel>
        .fetch(actionName: "OCI2_0300", methodName: "hybridStatus", params: params)
        { response in
            if response.rtnCode == 1
            {
                self.hybridStatus = response.models
            }
            
            if let find = self.hybridStatus?.first(where: { $0.progId == id} )
            {
                callback(find)
            }
            else
            {
                let nonFind = OCI2_0300_HybridStatusModel()
                callback(nonFind)
            }
        }
    }
}
*/

/*
extension MemoryTemp {
    
    static func registerContentProvider(deviceToken: String, completion: @escaping () -> ())
    {
        let param = ["ACTION_NAME": "OCI2_1400",
                     "METHOD_NAME": "RegisterTokenID",
                     "APP_KEY": Bundle.main.bundleIdentifier!,
                     "PLATFORM": "1",
                     "TOKEN_ID": deviceToken,
                     "IS_NOTIFY": "Y",
                     "MACHINE_ID": Keychain.UUID()]
        
        APIManager.request(Environment.OCWebservice, method: .post, parameters: param).responseJSON
            { (resp) in
                DispatchQueue.main.async {
                    completion()
                }
            }
    }
}
*/

//post
//cache 
/*
extension MemoryTemp
{
    final func preLoadGuaranteeModels()
    {
        APIResponse<OCI2_0700_QueryGuaranteeStatusListModel>.fetch(actionName: "OCI2_0700", methodName: "queryGuaranteeStatusList")
        { response in
            self.guaranteeModels = response.models
        }
    }
}

extension MemoryTemp
{
    final func userSignOut()
    {
        APIResponse<SignOutModel>.signOut()
        UIDevice.cleanCookies()
        userId = nil
        userInfo = nil
        errTimes = 0
        guaranteeModels = nil
        claimsSchedule = nil
    }
}
*/
